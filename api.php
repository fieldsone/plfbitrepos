<?php
// db login
require("dbinfo.php");

// SET URL
///////////////////////////////////////////
$url = "http://aaf.amazingrobotandsons.com";

// SET FROM EMAIL
$fromEmail = "<confirm@aafintl.com>";


$email = filter_var($_GET['email'], FILTER_SANITIZE_EMAIL);
// check if email was submitted via get
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
	// Valid, continue

	// check if email is in system, if yes return 1

	// Connect to database
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	//print($email + "<hr />");
	$sql = "SELECT * FROM emails WHERE email=\"$email\" and validated=1";
	$result = $conn->query($sql);
	$num_rows = mysqli_num_rows($result);

	
	// if email not found, return 0 and send verification email and save to database
	if($num_rows == 0)
	{
		// make token
		$token = getToken(15);
		
		// DB entry
		$sql2 = "INSERT INTO emails (email, token) VALUES (\"$email\",\"$token\")";
		if ($conn->query($sql2) === TRUE) {
			//echo "email sent, record added";
		} else {
			//echo "Error: " . sql2 . "<br>" . $conn->error . "<br />";
		}

		// Send Email	
		$imgpath = "http://aaf.amazingrobotandsons.com/images";
		$msg = <<<EOD
<html>
<head>
<meta charset="utf-8">
<title>Email Template</title>
	<style>

		body, .outter_container {
			background-color: #EEEEEE;
			font-family: "Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif";
			font-size: 22px;
		}

		.inner_content {
			display:block;
			padding:0;
			width: 600px;
			background-color: #FFF;
			border: 0;
		}

		.msg {
			display: block;
			margin: 0 auto 0;
			width: 80%;
			text-align: justify;
			margin-top:40px;
			margin-bottom: 40px;
		}
	</style>
</head>

<body>
<div class="outter_container">
	<div class="msg">You received this email at $email because you have requested access to the AAF Flanders Air Filtration Reference &amp; Product Guide. Once your email has been verified, you will continue to have access to the catalog through your registered device.</div>
	<div class="msg"><a href="$url/verify.php?token=$token&email=$email">Click here to verify your email address.</a></div>
</div>

</body>
</html>
EOD;



		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: ' . $fromEmail . "\r\n";

		// send email
		mail($email,"AFF Email Test",$msg,$headers);
		
		print("status=0");
	}else{
		// Found
		print("status=1");
	}

} else {
	// Not Valid
	print("status=-1");
}
	
function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd > $range);
    return $min + $rnd;
}

function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
    }

    return $token;
}
	
?>