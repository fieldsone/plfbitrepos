<?php 

	require("../dbinfo.php"); 

	// Connect to database
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}

	// (try to) run your query.
	$result = $conn->query('SELECT * FROM emails WHERE validated=1 ORDER BY timestamp DESC');

	if (! $result){ 
		// probably a syntax error in your SQL, 
		// but could be some other error
		throw new Db_Query_Exception("DB Error: " . mysql_error()); 
	}

	


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>AAF Mobile Experience App - Verified Email Addresses</title>
	<Style>
	
		table {border:1px solid black;}
		
		* { font-family: "Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif"; }
		
	</Style>
</head>

<body>

	<table border="1" cellpadding="5px" cellspacing="0">
		<tr><td>Email</td><td>subscribed</td><td>timestamp</td></tr>
<?php
		
	//zero-length results are usually a a special case    
	if ($result->num_rows > 0) {
		// our query returned at least one result. loop over results and do stuff.
		while($row = $result->fetch_assoc()) {
			$isSub = ($row['subscribe'] == 1) ? 'yes':'no';
			echo "<tr><td>" . $row['email'] . "</td><td>" . $isSub . "</td><td>" . $row['timestamp'] . "</td></tr>";
		}
	}else{
		echo "NONE";
	}
		
?>			
	</table>
	
</body>
</html>