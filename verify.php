<?php
// db login
require("dbinfo.php");

$token = filter_var($_GET["token"], FILTER_SANITIZE_STRING);
$email = filter_var($_GET["email"], FILTER_SANITIZE_STRING);

if($token != "" && $email != ""){	

	// Connect to database
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	if($_GET["subscribe"] == "on")
	{
		$sql = "UPDATE emails SET validated=1, subscribe=1 WHERE token=\"$token\" AND email=\"$email\"";
	}else{
		$sql = "UPDATE emails SET validated=1, subscribe=0 WHERE token=\"$token\" AND email=\"$email\"";
	}
	
	$result = $conn->query($sql);
}

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>AAF Experience App - Verification</title>
	
<style>
	
	* {
		padding: 0;
		margin: 0;
		font-size: 10px;
		font-family: "Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif"
	}
	
	h1 {font-size: 4em; margin: 40px 20px 20px 20px;}
	h2 {font-size: 2.8em; margin: 20px 20px 0px 20px;}
	p { font-size: 2.2em; margin-left: 20px; }
	
	
	#pageContainer {
		display: block;
		width:100%;
	}
	
	#header { border-bottom: 5px solid #f3002e;}
	
	#innerHeader {
		display: block;
		max-width: 1140px;
		margin: 0 auto 0;
		height: 130px;
	}
	
	#logo { margin-top: 25px; margin-left: 20px;}
	
	#innerBody {
		display: block;
		max-width: 1140px;
		margin: 0 auto 0;
		min-height: 300px;
	}
	
	.check {
		width: 20px;
		height: 20px;
		margin-top: 5px;
		margin-right: 10px;
	}
	
	#footer { background: #606060; height: 130px;}
	
	#innerFooter {
		display: block;
		position: relative;
		max-width: 1140px;
		margin: 0 auto 0;
	}
	
	#footerLogo {
		display: block;
		float:left;
		height: 130px;
		width: 300px;
	}
		#footerLogo img {
			margin-left: 20px;
			margin-top: 40px;
		}
	
	#footerMenu {
		display: block;
		position: relative;
		float:left;
		width: 650px;
		height: 130px
	}
	
		#footerMenu ul { display: block; list-style: none; float: left; width: 650px; margin-top: 20px;}
		#footerMenu li { display: block; float:left; margin-left: 10px; padding-left:10px; border-left:1px solid white;}
		#footerMenu a { color:#FFF; text-decoration: none; font-size: 1.5em; margin-left: 10px;}
		#footerMenu li.first { border:none; margin-left:90px;} 
	
	#copyright {
		display: block;
		float:left;
		width:600px;
		color: white;
		padding:20px;
		font-size:1.3em;
		text-align: center;
	}
	
	#socialLinks {
		display: block;
		position: relative;
		float:right;
		width: 150px;
		height: 130px;
	}
	
	#socialLinks ul { display: block; list-style: none; margin-top: 35px;}
	
</style>
	
</head>

<body>
	<div id="pageContainer">
		<div id="header">
			<div id="innerHeader">
				<img id="logo" src="images/Logo.jpg" alt="AAF International Logo" />
			</div>
		</div>
		<div id="innerBody">
		<form action="#" name="myForm" id="myForm" method="get">
			<h1>Thank You. Your email has been verified.</h1>
			<p>When you return to the AAF Experience App, you will have access to the catalog.</p>
			
			<?php
				
				if($_GET["subscribe"] == "on"){
					$checked = "checked";
				}else{
					$checked = "";
				}
			
			?>
			
			<h2>Continue to hear from AAF Flanders</h2>
			<p><input class="check" type="checkbox" name="subscribe" <?php echo $checked; ?>  onClick="saveSub();" />I agree to receive future communications from AAF Flanders and AAF Interncational.</p>
			<input type="hidden" name="email" value="<?php echo $email; ?>" />
			<input type="hidden" name="token" value="<?php echo $token; ?>" />
			<input type="hidden" name="subscribeUpdate" value="true" />
		</form>	
		</div>
		<div id="footer">
			<div id="innerFooter">
				<div id="footerLogo">
					<img src="images/Logo_footer.jpg" alt="Footer AAF Logo" />
				</div>
				<div id="footerMenu">
					<ul>
						<li class="first"><a href="https://www.aafintl.com/en/residential/contact-us">Contact US</a></li>
						<li><a href="https://www.aafintl.com/en/residential/about-us/about-us/terms-and-conditions">Terms &amp; Conditions</a></li>
						<li><a href="https://www.aafintl.com/en/residential/about-us/about-us/privacy">Privacy</a></li>
						<li><a href="https://www.aafintl.com/en/residential/about-us/about-us/careers">Careers</a></li>
					</ul>
					<div id="copyright">© 2018 American Air Filter Company, Inc. This web site is property of American Air Filter Company, Inc. Any use of the text or images it contains, without permission of American Air Filter Company, Inc., is prohibited</div>
				</div>
				<div id="socialLinks">
					<ul>
						<li>
							<a href="https://business.facebook.com/AAFFlandersResidential"><img src="images/social_fb.jpg" /></a>
							<a href="https://twitter.com/aaf_intl"><img src="images/social_tw.jpg" /></a>
							<a href="https://www.instagram.com/american.air.filter"><img src="images/social_ig.jpg" /></a>
						</li>
						
					</ul>
				</div>
			</div>
			
		</div>
	</div>
	<script type="text/javascript">
	
	
		function saveSub(){
			document.getElementById("myForm").submit();
		}
		
	</script>
</body>
</html>